﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    float speed;
    List<Transform> waypoints;

    int currentWaypointIndex;

    public void SetWaypoints(List<Transform> waypoints)
    {
        this.waypoints = waypoints;
    }

    public void SetSpeed(float speed)
    {
        this.speed = speed;
    }

    // Start is called before the first frame update
    void Start()
    {
        transform.position = waypoints[0].position;
        currentWaypointIndex = 1;
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        
    }

    private void Move()
    {
        Vector3 targetPosition = waypoints[currentWaypointIndex].position;
        float movementDelta = speed * Time.deltaTime;

        // move sprite towards the target location
        transform.position = Vector2.MoveTowards(transform.position, targetPosition, movementDelta);

        if (Mathf.Approximately(transform.position.x, targetPosition.x) 
            && Mathf.Approximately(transform.position.y, targetPosition.y))
        {
            if (currentWaypointIndex == waypoints.Count - 1)
            {
                Destroy(gameObject);
            }
            else
            {
                currentWaypointIndex++;
            }
        }
    }
}
